#include <stdio.h>

void sub(int a, int b)
{
    printf("Sub: %d\n", a-b);
}
void add(int a, int b)
{
    printf("Sum: %d\n", a + b);
}
void div(int a, int b)
{
    printf("Div: %f\n", a / (float)b);
}
int max(int a, int b)
{
    if (a < b)
        return b;
    else
        return a;
}
void mul(int a, int b)
{
    printf("Mul: %f\n", a/(float)b);
}
void min(int a, int b)
{
    if(a>b){
        printf("Min: %d", b);
    }else{
        printf("Min: %d", a);
    }
}
int main()
{
    int a = 5;
    int b = 6;

    sub(a, b);
    add(a, b);
    div(a, b);
    printf("MAX: %d\n", max(a, b));
    mul(a,b);
    min(a,b);
    putchar('\n');

    // LN5
    // Dawid Wolek
    // Michal Polak

    return 0;
}
